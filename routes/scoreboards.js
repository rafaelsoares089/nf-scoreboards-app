var express = require('express');
var router = express.Router();
var ScoreBoard = require('./../models/scoreboard');

module.exports = function(router) {
  router.route('/scoreboards')
    .get(function(req, res, next) {
      ScoreBoard.find(function (err, scoreBoards) {
        if (err) res.status(500).json({message:err});
        res.json(scoreBoards);
      });
    })
    .post(function(req, res, next) {
      ScoreBoard.create(req.body, function(err, scoreboard){
        if(err) return res.status(500).json({message:err});
        return res.status(200).json(scoreboard);
      });
    });
}
