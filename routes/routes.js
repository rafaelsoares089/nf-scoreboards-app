var config = require('../config/config.json');

module.exports = function(router){

  router.get('/', function(req,res){
    res.json('nextframe games scoreboards api, salve salve meu guerreiro.');
  });

  router.use(function(req,res,next){
    if(req.method == 'GET')
      return next();

    var token = req.body.token || req.headers['token'];
    //console.log(token)
    if(token && token == config.secret){
      return next();
    } else {
      return res.json({message:"de a foda fora!"});
    }
  });


  require('./scores')(router);
  require('./scoreboards')(router);

  return router
};
