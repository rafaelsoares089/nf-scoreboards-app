var express = require('express');
var router = express.Router();
var Score = require('./../models/score');

module.exports = function(router){

  router.route('/scores')
    .get(function(req, res, next) {
        // Score.find(function (err, scores) {
        //   if (err) return res.status(500).json({message:err});
        //   return res.status(200).json(scores);
        // });

        Score.find({})
          .sort('-score')
          .limit(100)
          .exec(function (err, scores) {
            if (err) return res.status(500).json({message:err});
            return res.status(200).json(scores);
          });
      })
    .post(function(req, res, next) {
      var score = req.body;
        Score.find({})
          .limit(5)
          .sort('-score')
          .exec(function(err, scores){
            if(scores.length >= 5) {
              if (score.score > scores[scores.length-1].score) {
                scores[scores.length-1].remove();
              } else {
                return res.status(200).json(score);
              }
            }
            Score.create(req.body, function (err, score) {
              if (err) return res.status(500).json({message:err});
              return res.status(200).json(score);
            });
          });

    });
};
