var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scoreboardSchema = new Schema({
  game: String,
  scoreboardId: Number,
});

module.exports = mongoose.model('Scoreboard', scoreboardSchema);
