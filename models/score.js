var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scoreSchema = new Schema({
  name: {type:String, required: true},
  score: {type:Number, required: true},
  scoreboardId: {type:Number, required: true}
});

module.exports = mongoose.model('Score', scoreSchema);
